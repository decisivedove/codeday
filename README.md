# CodeDay

Code Day 2021 project
Discord Bot

## Instructions
1. Install dependencies: `python3 -m pip install -U discord.py requests minesweeperPy`
2. Rename `auth_template.py` to `auth.py` and insert your bot token in the `self.token` variable
3. Run `python3 bot.py`