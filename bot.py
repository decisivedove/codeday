import discord
import time
import random
import minesweeper
from discord.ext import commands
from discord.utils import get
import sys
from auth import auth

env = auth()

#=== password sys ===============
def curTime():
    return round(time.time())

THE_CODE = random.randint(100000,999999)
lastTime = curTime()

def regenCode():
    global THE_CODE, lastTime
    THE_CODE = random.randint(100000,999999)
    lastTime = curTime()

def checkCode(password):
    global THE_CODE, lastTime
    if curTime() - lastTime >= 300:
        regenCode()
    return str(password) == str(THE_CODE)
#=======================

intent = discord.Intents.default()
intent.typing = False
intent.presences = False
intent.members = True

bot = commands.Bot(command_prefix = env.get_prefix(), intents = intent)

@bot.event
async def on_ready():
    print ("Connected to the Discord's server as " + bot.user.name)
    print ("Success")

@bot.event
async def on_member_join(member):
    channel = discord.utils.get(member.guild.text_channels, name="general")
    await channel.send(f"Welcome {member.mention}!!! :partying_face: ")

@bot.event
async def on_member_remove(member):
    channel = discord.utils.get(member.guild.text_channels, name="general")
    await channel.send("Adios. Sayonara. Bye.!!! " + member.name + " :sob: ")
    
@bot.command()
async def latency(ctx):
    await ctx.send (f'latency = {round(bot.latency * 1000)}ms')


@bot.command()
async def roll(ctx):
    num = random.randint(1, 20)
    await ctx.send("Rolled: " + str(num))
    if num == 1:
        await ctx.send("CRITICAL FAIL!!!")
    elif num == 20:
        await ctx.send("CRITICAL SUCCESS!!!")

@bot.command()
async def dp(ctx):
    global THE_CODE
    checkCode(0)
    if ctx.message.author.guild_permissions.administrator:
        await ctx.author.send("SSHHHHHH: " + str(THE_CODE))

@bot.command()
async def shutdown(ctx, arg):
    if checkCode(arg):
        bot.logout()
        exit()
    else:
        await ctx.send("WRROOOOOOOOOONNNNNGGGGGG!!!")

def is_integer(n):
    try:
        int(n)
        return True
    except ValueError:
        return False

# minesweeper
@bot.command()
async def mine(ctx, size=None, mines=None):
    global env
    if size is None or mines is None:
        await ctx.send(f"Correct use of this command: `{env.prefix}mine <size 1-10> <mines count>`")
        return
    if not (is_integer(size) or is_integer(mines)):
        await ctx.send("Size and mine count must be a number")
        return
    if int(size) > 10:
        await ctx.send("Maximum size is 25!")
    if int(size) < 0 or int(mines) < 0:
        await ctx.send("Size or mine count cannot be negative!")
        return
    if int(size) == 0 or int(mines) == 0:
        await ctx.send("Size or mine count cannot be 0!")
        return
    if int(mines) > int(size) * int(size):
        await ctx.send("Mine count cannot be larger than size!")
        return

    emojis = {
        " ": ":zero:",
        "1": ":one:",
        "2": ":two:",
        "3": ":three:",
        "4": ":four:",
        "5": ":five:",
        "6": ":six:",
        "7": ":seven:",
        "8": ":eight:",
        "M": ":boom:"
    }

    minefield = minesweeper.Generator(int(size), int(size)).generate_raw(int(mines))

    message = ""

    for i in minefield:
        for j in i:
            message += "||" + str(emojis[str(j)]) + "|| "
        message += "\n"

    game = "Generated " + "**" + str(size) + " by " + str(size) + "**" + " minefield with " + str(mines) + "**" + " mines" + "**" + "\n" + message
    await ctx.send(game)

bot.run(env.get_token())
